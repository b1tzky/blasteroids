import pyglet, random, util


class Velocity(object):
    """Holds x and y values correlating to the velocity of an object.
    """

    x = 0
    y = 0

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    @classmethod
    def random(self, limit=100):
        return Velocity(random.randrange(-limit, limit), random.randrange(-limit, limit))

    def __div__(self, value):
        if isinstance(value, Velocity):
            self.x /= value.x
            self.y /= value.y
        else:
            self.x /= value
            self.y /= value
        return self

    def __add__(self, value):
        if isinstance(value, Velocity):
            self.x += value.x
            self.y += value.y
        else:
            self.x += value
            self.y += value
        return self

    def __radd__(self, value):
        return self.__add__(value)

    def __mul__(self, value):
        if isinstance(value, Velocity):
            self.x *= value.x
            self.y *= value.y
        else:
            self.x *= value
            self.y *= value
        return self

    def __rmul__(self, value):
        return self.__mul__(value)

class GameObject(pyglet.sprite.Sprite):
    """A generic object for all game objects that need interaction.

    Attributes:
        velocity = Represents this objects velocity through space
        dead = Whether or not we should remove this object on a scenes update()
        new_objects = A list containing any new objects we would like to add to the objects container
        event_handlers = Any input event handlers we'd like to add to a given object
    """

    velocity = None
    dead = False
    new_objects = []
    event_handlers = []

    def __init__(self, *args, **kwargs):
        super(GameObject, self).__init__(*args, **kwargs)
        self.velocity = Velocity()

    def update(self, dt):
        if not self.dead and not self.image:
            return

        self.x += self.velocity.x * dt
        self.y += self.velocity.y * dt

        self.check_bounds()

    def check_bounds(self):
        """Check to see if the object has left the screen.
        If it does, move it to the opposing side.
        """

        min_x = -self.image.width/2
        min_y = -self.image.height/2
        max_x = 800 + self.image.width/2
        max_y = 600 + self.image.height/2

        if self.x < min_x:
            self.x = max_x
        elif self.x > max_x:
            self.x = min_x

        if self.y < min_y:
            self.y = max_y
        elif self.y > max_y:
            self.y = min_y

    def collides_with(self, other_object):
        """Whether or not two objects have collided.
        It is intended to overload this to determine whether two objects
        should even bother calculating the distance between them.
        """

        if self.dead or other_object.dead or not self.image:
            return False

        collision_distance = self.image.width/2 + other_object.image.width/2
        actual_distance = util.distance(self.position, other_object.position)

        return actual_distance <= collision_distance
