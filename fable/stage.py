import pyglet
import logging
from game import resources

logger = logging.getLogger(__name__)

class Stage(object):
    window = None
    scenes = {}
    event_stack_size = 0
    controller = None

    def __init__(self, name='Blasteroids'):

        logger.info("starting %s" % name)

        self.window = pyglet.window.Window(800, 600, name)

        pyglet.clock.schedule_interval(self.update, 1/120.0)
        # self.window.push_handlers(pyglet.window.event.WindowEventLogger())

    def give_control_to(self, name):
        if self.controller:
            logger.info("removing %d handlers from %s" % (
                len(self.controller.event_handlers), self.controller))
            for handler in self.controller.event_handlers:
                self.window.remove_handlers(handler)

        self.controller = self.scenes[name]

        logger.info("adding %d handlers to %s" % (
            len(self.controller.event_handlers), self.controller))
        for handler in self.controller.event_handlers:
            self.window.push_handlers(handler)

    def activate(self, name):
        logger.info("activating %s" % name)
        self.scenes[name].active = True

    def deactivate(self, name):
        logger.info("deactivate %s" % name)
        self.scenes[name].active = False

    def add(self, name, scene):
        logger.info("adding %s" % name)
        self.scenes[name] = scene()

    def play(self):
        logger.info("startup done, running")
        pyglet.app.run()

    def update(self, dt):
        for i in self.scenes:
            if self.scenes[i].active:
                self.scenes[i].update(dt)

    def draw(self):
        self.window.clear()

        # draw in the background of the game
        x = 0
        y = 0
        while(y < self.window.height):
            while(x < self.window.width):
                resources.background.blit(x, y)
                x += 256
            x = 0
            y += 256

        for i in self.scenes:
            if self.scenes[i].active:
                self.scenes[i].draw()

stage = Stage()

class Scene(object):
    active = False
    stage = None
    window = None
    objects = []
    layers = 0
    event_handlers = []
    event_handler_backup = []

    def __str__(self):
        return self.__class__.__name__

    def __init__(self):
        logger.info("created %s" % self)
        self.window = stage.window
        self.layers = [pyglet.graphics.Batch() for i in xrange(self.layers)]

    def set_active(self, active=True):
        self.active = active

    def take_control(self):
        self.stage.controller = self

    def check_for_collisions(self):
        for i in xrange(len(self.objects)):
            for j in xrange(len(self.objects)):
                obj_1 = self.objects[i]
                obj_2 = self.objects[j]

                if not obj_1.dead and not obj_2.dead:
                    if obj_1.collides_with(obj_2):
                        obj_1.handle_collision_with(obj_2)
                        obj_2.handle_collision_with(obj_1)

    def update(self, dt):
        for obj in [obj for obj in self.objects if not obj.dead]:
            obj.update(dt)

    def draw(self):
        for batch in self.layers:
            batch.draw()
