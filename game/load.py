import pyglet, random
import resources, objects
from fable import util


def asteroids(num_asteroids, player_position=None, batch=None):
    asteroids = []
    for i in xrange(num_asteroids):
        x = random.randint(0, 800)
        y = random.randint(0, 600)

        if player_position:
            while util.distance((x, y), (400, 300)) < 200:
                x = random.randint(0, 800)
                y = random.randint(0, 600)

        asteroid = objects.Asteroid(x=x, y=y, batch=batch)
        asteroid.velocity.random(limit=100)

        asteroids.append(asteroid)
    return asteroids


def item(player_position, batch=None):
    x = random.randint(0, 800)
    y = random.randint(0, 600)

    while util.distance((x, y), player_position) < 200:
        x = random.randint(0, 800)
        y = random.randint(0, 600)

    item = objects.Item(x=x, y=y, batch=batch)

    return item
