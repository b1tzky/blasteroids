import pyglet, math, random, resources
from pyglet.sprite import Sprite
from pyglet.window import key
import logging

from fable import util
from fable.gameobject import (GameObject, Velocity)
from fable.stage import stage

logger = logging.getLogger(__name__)


class SpaceObject(GameObject):
    """We're giving these objects some default values so that we can decide
    whether or not they should collide. It should make collides_with() a little
    faster but I'm not sure exactly how fast
    """
    reacts_to_bullets = False
    is_bullet = False
    is_item = False
    gets_items = False


class Asteroid(SpaceObject):
    size = 2
    """Decreases each time the asteroid is hit"""

    reacts_to_bullets = True

    def __init__(self, size=2, velocity=None, *args, **kwargs):
        self.size = size

        super(Asteroid, self).__init__(
            random.choice(resources.asteroids[self.size]), *args, **kwargs)

        self.velocity = velocity or Velocity.random()
        self.rotation = random.randint(0, 360)
        self.rotate_speed = random.random() * 100.0 - 50.0

    def update(self, dt):
        super(Asteroid, self).update(dt)
        self.rotation += self.rotate_speed * dt

    def collides_with(self, other_object, *args, **kwargs):

        if not other_object.is_bullet:
            return False

        return super(Asteroid, self).collides_with(other_object)

    # TODO: splosions
    def handle_collision_with(self, other_object):
        name = other_object.__class__

        if isinstance(other_object, Bullet) or isinstance(other_object, Ship):
            logger.info("asteroid: got hit")

            self.dead = True

            if isinstance(other_object, Bullet):
                # print(stage.player.score)
                stage.player.score += 1

            # upon death, spawn a few new and smaller asteroids
            if self.size > 0:
                for i in xrange(random.randint(2, 4)):
                    asteroid = Asteroid(
                        x=self.x, y=self.y, size=self.size-1, batch=self.batch,
                        velocity=Velocity.random(100))

                    self.new_objects.append(asteroid)


class Score(object):
    score = 0

    def __add__(self, value):
        self.score += value
        self.render()
        return self

    def __radd__(self, value):
        self.__add__(value)

    def zero(self):
        self.score = 0
        self.render()

    def render(self):
        stage.scenes['action'].score_label.text = "Score: %s" % self.score


class Player(object):
    lives = 3
    dead = False
    life_sprites = []

    def __init__(self):
        self.score = Score()

    def clear_lives(self):
        for life in self.life_sprites:
            life.delete()

    def render_lives(self):
        pass
        # self.clear_lives()
        #
        # self.life_sprites = []
        # for i in xrange(self.lives):
        #     life = pyglet.sprite.Sprite(
        #         img=resources.life_small,
        #         x=785-i*25, y=585,
        #         batch=stage.scenes['action'].layers[2])
        #
        #     self.life_sprites.append(life)


class Ship(SpaceObject):
    reacts_to_bullets = False
    thrust = 500.0
    rotate_speed = 200.0
    gets_items = True
    radius = 0
    gun = None

    images = dict(on=None, off=None)

    def __init__(self, *args, **kwargs):
        self.images['on'] = resources.ship_on
        self.images['off'] = resources.ship_off

        super(Ship, self).__init__(img=self.images['off'], *args, **kwargs)

        self.radius = self.image.width / 2

        self.gun = Gun(self)

        self.key_handler = key.KeyStateHandler()
        self.event_handlers = [self, self.key_handler]

    def on_key_press(self, symbol, modifiers):
        if symbol == key.SPACE and not self.dead:
            self.gun.fire()

    def reset(self):
        self.dead = False
        self.x = 400
        self.y = 300
        self.rotation = -90
        self.velocity = Velocity()

    def update(self, dt):
        super(Ship, self).update(dt)

        if self.key_handler[key.LEFT]:
            self.rotation -= self.rotate_speed * dt

        if self.key_handler[key.RIGHT]:
            self.rotation += self.rotate_speed * dt

        if self.key_handler[key.UP]:
            self.image = self.images['on']

            angle_radians = -math.radians(self.rotation)

            self.velocity.x += math.cos(angle_radians) * self.thrust * dt
            self.velocity.y += math.sin(angle_radians) * self.thrust * dt
        else:
            self.image = self.images['off']

        # still not sure if i want the down key to function this ways
        if self.key_handler[key.DOWN]:
            self.velocity.x *= 0.94
            self.velocity.y *= 0.94

    def collides_with(self, other_object, *args, **kwargs):

        # if not self.gets_items and other_object.is_item:
        #     return False

        return super(Ship, self).collides_with(other_object)

    def handle_collision_with(self, other_object):
        if isinstance(other_object, Asteroid):
            self.dead = True
            logger.info("ship: hit an asteroid")
            if stage.player.lives > 1:
                stage.player.lives -= 1
                stage.player.render_lives()

                stage.activate("crash")
                stage.give_control_to("crash")
            else:
                stage.activate("gameover")
                stage.give_control_to("gameover")

        if isinstance(other_object, Item):
            if other_object.item_type == 0:
                logger.info("picked up super gun")
                self.gun = SuperGun(self)
            elif other_object.item_type == 1:
                logger.info("picked up extra life")
                if stage.player.lives < 10:
                    stage.player.lives += 1
                    stage.player.render_lives()


class Gun(object):
    bullet_speed = 700.0
    ship = None

    def __init__(self, ship):
        self.ship = ship

    def launch_bullet(self, spread=0):
        angle_radians = -math.radians(self.ship.rotation + spread)
        bullet = Bullet(
            self.ship.x + math.cos(angle_radians) * self.ship.radius,
            self.ship.y + math.sin(angle_radians) * self.ship.radius,
            batch=self.ship.batch)
        bullet.rotation = self.ship.rotation + spread
        bullet.velocity.x = self.ship.velocity.x + math.cos(angle_radians) * self.bullet_speed
        bullet.velocity.y = self.ship.velocity.y + math.sin(angle_radians) * self.bullet_speed
        self.ship.new_objects.append(bullet)

    def fire(self):
        logger.info("gun: fired a bullet")
        self.launch_bullet()


class SuperGun(Gun):
    def fire(self):
        super(SuperGun, self).fire()
        self.launch_bullet(-5)
        self.launch_bullet(5)


class Bullet(SpaceObject):
    is_bullet = True

    def __init__(self, *args, **kwargs):
        super(Bullet, self).__init__(resources.bullet_image, *args, **kwargs)
        pyglet.clock.schedule_once(self.die, 0.5)

    def die(self, dt=None):
        self.dead = True

    def collides_with(self, other_object, *args, **kwargs):

        if not other_object.reacts_to_bullets:
            return False

        return super(Bullet, self).collides_with(other_object)

    def handle_collision_with(self, other_object):
        if isinstance(other_object, Asteroid):
            self.die()


class Item(SpaceObject):
    is_item = True
    item_type = None

    def __init__(self, *args, **kwargs):
        self.item_type = random.choice([0,1])

        if self.item_type == 0:
            image = resources.item_weapon
        if self.item_type == 1:
            image = resources.item_life

        super(Item, self).__init__(image, *args, **kwargs)

    def collides_with(self, other_object):
        if not other_object.gets_items:
            return False

        super(Item, self).collides_with(other_object)

    def handle_collision_with(self, other_object):
        if isinstance(other_object, Ship):
            self.dead = True
