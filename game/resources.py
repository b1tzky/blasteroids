import pyglet
from pyglet.resource import image


pyglet.resource.path = ['resources']
pyglet.resource.reindex()

# import fonts
pyglet.font.add_file('resources/fonts/kenvector_future_thin.ttf')

def center(image):
    image.anchor_x = image.width/2
    image.anchor_y = image.height/2
    return image

# background = image("backgrounds/black.png")
background = image("backgrounds/blue.png")
# background = image("backgrounds/purple.png")
# background = image("backgrounds/darkPurple.png")

ship_on = center(image("ship_on.png", False, False, 90))
ship_off = center(image("ship_off.png", False, False, 90))
item_weapon = center(image("icons/powerup.png"))
item_life = center(image("icons/life.png"))
life_small = center(image("icons/life_small.png"))
bullet_image = center(image("bullet.png", False, False, 90))
asteroids = [
    [
        center(image("asteroids/small/1.png")),
        center(image("asteroids/small/2.png")),
    ],
    [
        center(image("asteroids/medium/1.png")),
        center(image("asteroids/medium/2.png")),
    ],
    [
        center(image("asteroids/large/1.png")),
        center(image("asteroids/large/2.png")),
        center(image("asteroids/large/3.png")),
        center(image("asteroids/large/4.png")),
    ],
]
