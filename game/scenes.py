import pyglet, logging, random, resources
from pyglet.window import key
import load, objects, logger

from objects import Player, Ship, Asteroid

logger = logging.getLogger(__name__)

from fable.stage import stage
from fable.stage import Scene


class StartScene(Scene):
    layers = 2

    def __init__(self, *args, **kwargs):

        super(StartScene, self).__init__(*args, **kwargs)

        self.title_text = pyglet.text.Label(text="BLASTEROIDS!", font_size=32,
                                        font_name='KenVector Future Thin', x=400, y=300,
                                        anchor_x='center', batch=self.layers[1])

        self.subtext = pyglet.text.Label(text="PRESS ANY KEY TO START", font_size=14,
                                        font_name='KenVector Future Thin', x=400, y=250,
                                        anchor_x='center', batch=self.layers[1])

        self.objects = load.asteroids(12, None, self.layers[0])
        self.event_handlers = [self]

    def on_key_press(self, symbol, modifiers):
        stage.activate("action")
        stage.give_control_to("action")
        stage.deactivate("start")


class ActionScene(Scene):
    # there should be a better way to do this
    # as of Scene.__init__(), layers is transformed to a list
    # of 3 Batch()es
    layers = 3

    # scene variables
    num_asteroids = 1

    victory = False

    font = 'KenVector Future Thin'

    def __init__(self, *args, **kwargs):
        super(ActionScene, self).__init__(*args, **kwargs)

        self.score_label = pyglet.text.Label(font_size=12, font_name=self.font,
                                             x=10, y=580, batch=self.layers[2])

        self.ship = self.new_ship()

        self.key_handler = key.KeyStateHandler()
        self.event_handlers = [self.key_handler] + self.ship.event_handlers

        self.reset()

    def new_ship(self):
        return objects.Ship(batch=self.layers[1])

    def restart(self):
        self.ship = self.new_ship()
        self.event_handlers = [self.key_handler] + self.ship.event_handlers
        self.reset()

    def reset(self, new_ship=False):
        # stage.player.score.render()
        self.victory = False
        stage.player.render_lives()
        asteroids = load.asteroids(self.num_asteroids, self.ship.position, self.layers[1])
        self.objects = [self.ship] + asteroids

        self.ship.reset()

    def update(self, dt):

        self.check_for_collisions()

        # objects can add other objects to the stage`
        to_add = []
        asteroids_remaining = 0

        if random.randrange(10000) < 10:
            logger.info("spawning item")
            to_add += [load.item(self.ship.position, self.layers[1])]

        # update all the objects
        for obj in self.objects:
            obj.update(dt)

            # if anyone would like to add new objects
            to_add.extend(obj.new_objects)
            obj.new_objects = [] # tidy up

            # counting asteroids
            if isinstance(obj, objects.Asteroid):
                asteroids_remaining += 1

        victory = not self.victory and (not self.ship.dead and asteroids_remaining == 0)

        # if there are no asteroids remaining, you win
        if victory:
            self.num_asteroids += 1
            stage.player.score += 10
            stage.deactivate("action")
            stage.activate("victory")
            stage.give_control_to("victory")

        # go through all the objects again
        for to_remove in [obj for obj in self.objects if obj.dead]:

            # remove the object from video memory
            try:
                to_remove.delete()
            except AttributeError:
                # print(to_remove)
                pass

            # and remove it from the game objects
            self.objects.remove(to_remove)

        self.objects.extend(to_add)


class VictoryScene(Scene):
    layers = 1

    def __init__(self, *args, **kwargs):
        super(VictoryScene, self).__init__(*args, **kwargs)

        self.title_text = pyglet.text.Label(text="VICTORY!", font_size=32,
                                        font_name='KenVector Future Thin', x=400, y=300,
                                        anchor_x='center', batch=self.layers[0])

        self.subtext = pyglet.text.Label(text="PRESS ANY KEY TO CONTINUE", font_size=14,
                                        font_name='KenVector Future Thin', x=400, y=250,
                                        anchor_x='center', batch=self.layers[0])

        self.event_handlers = [self]

    def on_key_press(self, symbol, modifiers):
        # kill the screen
        stage.deactivate("victory")

        # reset a few things on the action scene
        action = stage.scenes["action"]
        action.reset()

        # reactivate the action scene
        stage.activate("action")
        stage.give_control_to("action")


class CrashScene(Scene):
    layers = 1

    def __init__(self, *args, **kwargs):
        super(CrashScene, self).__init__(*args, **kwargs)

        self.title_text = pyglet.text.Label(text="YOU CRASHED!", font_size=32,
                                        font_name='KenVector Future Thin', x=400, y=300,
                                        anchor_x='center', batch=self.layers[0])

        self.subtext = pyglet.text.Label(text="PRESS ANY KEY TO RESTART", font_size=14,
                                        font_name='KenVector Future Thin', x=400, y=250,
                                        anchor_x='center', batch=self.layers[0])

        self.event_handlers = [self]

    def on_key_press(self, symbol, modifiers):
        stage.deactivate("crash")
        stage.scenes["action"].restart()
        stage.give_control_to("action")


class GameOverScene(Scene):
    layers = 1

    def __init__(self, *args, **kwargs):
        super(GameOverScene, self).__init__(*args, **kwargs)

        self.title_text = pyglet.text.Label(text="GAME OVER", font_size=32,
                                        font_name='KenVector Future Thin', x=400, y=300,
                                        anchor_x='center', batch=self.layers[0])

        self.subtext = pyglet.text.Label(text="PRESS ANY KEY TO START OVER", font_size=14,
                                        font_name='KenVector Future Thin', x=400, y=250,
                                        anchor_x='center', batch=self.layers[0])

        self.event_handlers = [self]

    def on_key_press(self, symbol, modifiers):
        action = stage.scenes["action"]

        stage.player.score.zero()
        stage.player.lives = 3

        action.num_asteroids = 1

        stage.deactivate("gameover")
        stage.deactivate("action")

        action.restart()

        stage.activate("start")
        stage.give_control_to("start")
