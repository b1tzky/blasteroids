#!/usr/bin/env python
import pyglet, random, logging

from fable.stage import stage
from game.scenes import (StartScene, ActionScene, VictoryScene, CrashScene, GameOverScene)
from game import objects

logger = logging.getLogger(__name__)

stage.player = objects.Player()

stage.add("start", StartScene)
stage.add("action", ActionScene)
stage.add("victory", VictoryScene)
stage.add("crash", CrashScene)
stage.add("gameover", GameOverScene)

stage.activate("start")
stage.give_control_to("start")

@stage.window.event
def on_draw():
    stage.draw()

try:
    if __name__ == '__main__':
        stage.play()
except (KeyboardInterrupt,):
    logger.info(" caught ^C, quitting")
    quit()
